from flask_restful import Resource
from marshmallow import (
    Schema, fields,
)
from flask_jwt_extended import jwt_required
from ..auth import get_current_user_or_abort
from ..models import (
    db,
    Violation,
)
from ..signals import new_violation
from ..utils.io import load_from_request


class ViolationsSchema(Schema):
    spot_id = fields.Int(required=True)
    photo_id = fields.String(required=True)
    comment = fields.String()


class Violations(Resource):
    @jwt_required
    def post(self):
        user = get_current_user_or_abort()
        data = load_from_request(ViolationsSchema)

        violation = Violation(customer_id=user.id, **data)
        with db.auto_commit() as sess:
            sess.add(violation)

        new_violation.send(violation)

        return {'message': 'Violation created successfully.'}
