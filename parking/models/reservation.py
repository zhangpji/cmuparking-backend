import datetime as dt
from time import sleep
import enum
from flask import current_app
from sqlalchemy import (
    Column,
    Integer, DateTime, Enum, String,
    ForeignKey)
from .base import db, Base
from ..jobs import (
    rq, msg,
    schedule_or_enqueue,
    get_obj_from_id,
    display_led, clear_led)
from ..signals import new_reservation
from ..utils.shoyu import strftime_local


class ReservationState(enum.Enum):
    PENDING = enum.auto()    # waiting for the spot
    WAITING = enum.auto()    # waiting for the user to check in
    STARTED = enum.auto()    # user has checked in
    ENDED = enum.auto()      # user has checked out
    CANCELLED = enum.auto()  # user has cancelled the reservation


# Valid state transtions
VALID_TRANSITIONS = {
    ReservationState.PENDING: [ReservationState.WAITING,
                               ReservationState.CANCELLED],

    ReservationState.WAITING: [ReservationState.STARTED,
                               ReservationState.CANCELLED],

    ReservationState.STARTED: [ReservationState.ENDED],

    ReservationState.ENDED: [],
    ReservationState.CANCELLED: []
}


class Reservation(Base):
    __tablename__ = 'reservation'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    updated_at = Column(DateTime, default=dt.datetime.utcnow,
                        onupdate=dt.datetime.utcnow)

    state = Column(Enum(ReservationState), default=ReservationState.PENDING)

    start = Column(DateTime, nullable=False)
    end = Column(DateTime, nullable=False)
    checkout_at = Column(DateTime)

    customer_id = Column(Integer, ForeignKey('user.id'), nullable=False)

    location_id = Column(Integer, ForeignKey('location.id'), nullable=False)
    spot_id = Column(Integer, ForeignKey('spot.id'))

    jobs = db.relationship('Job', backref='reservation',
                           lazy='dynamic')

    def __repr__(self):
        return f'<Reservation {self.id} by {self.customer.name}>'

    # FIXME Validate the reservation before adding it to db
    @classmethod
    def create(cls, *args, **kwargs):
        with db.auto_commit() as sess:
            reservation = cls(*args, **kwargs)
            sess.add(reservation)

        reservation._post_create()

        return reservation

    def extend(self, new_end):
        with db.auto_commit() as sess:
            self._pre_extend()
            self.end = new_end
            self._post_extend(sess)

    # State transtions
    def _update_state(self, new_state):
        if (new_state in VALID_TRANSITIONS.get(self.state)):
            self.state = new_state
        else:
            raise ValueError('invalid state transition '
                             f'from {self.state} '
                             f'to {new_state}')

    def assign_spot(self):
        spot_id = self.location.get_available_spot(self.start)
        self._update_state(ReservationState.WAITING)
        self.spot_id = spot_id

    def checkin(self):
        self._update_state(ReservationState.STARTED)

    def checkout(self):
        self._update_state(ReservationState.ENDED)
        self.checkout_at = dt.datetime.utcnow()
        self._post_checkout()

    def cancel(self):
        self._update_state(ReservationState.CANCELLED)
        self._post_cancel()

    # Hooks
    def _post_create(self):
        padding = self.location.conf.reservation_padding
        assigning_time = self.start - dt.timedelta(minutes=padding)

        job_times = [
            (assign_spot, assigning_time, JobType.OTHER),
            (auto_checkin, self.start, JobType.OTHER),
            (auto_checkout, self.end, JobType.END)]

        with db.auto_commit() as sess:
            for (job, time, type) in job_times:
                job_id = schedule_or_enqueue(job, time, self.id)
                job_record = Job(id=job_id,
                                 type=type,
                                 reservation_id=self.id)
                sess.add(job_record)

        new_reservation.send(self)

    def _pre_extend(self):
        end_jobs = (self
                    .jobs
                    .filter_by(
                        type=JobType.END,
                        valid=True).all())

        for job in end_jobs:
            job.cancel()

    def _post_extend(self, sess):
        job_id = schedule_or_enqueue(auto_checkout, self.end, self.id)
        job_record = Job(id=job_id, type=JobType.END,
                         reservation_id=self.id)
        sess.add(job_record)

    def _post_checkout(self):
        ip_addr = self.spot.ip_addr
        clear_led.queue(ip_addr)
        for job in self.jobs.all():
            job.cancel()

    def _post_cancel(self):
        self._post_checkout()

    # Misc
    def send_to_led(self):
        if self.is_alive() and self.spot_id:
            ip_addr = self.spot.ip_addr
            start = self.start
            end = self.end
            plate_number = self.customer.plate_number
            tz = self.location.conf.time_zone

            display_led(ip_addr, 1, plate_number)
            sleep(0.5)
            display_led(ip_addr, 2,
                        (f'{strftime_local(start, tz, "%H:%M")}'
                         f'-{strftime_local(end, tz, "%H:%M")}'))

    def notify_user_start(self):
        to_number = self.customer.phone_number
        delta = self.start - dt.datetime.utcnow()
        if to_number:
            message_body = (f'Your reservation at {self.location.name}'
                            f' spot {self.spot.name} will start in'
                            f' {delta.total_seconds() // 60} minutes.')
            msg.send_sms(to_number, message_body)

    def is_alive(self):
        return (self.state is not ReservationState.ENDED
                and
                self.state is not ReservationState.CANCELLED)


class JobType(enum.Enum):
    END = enum.auto()    # jobs related to the end of a reservation
    OTHER = enum.auto()  # other jobs


class Job(Base):
    __tablename__ = 'job'

    id = Column(String(50), primary_key=True)
    type = Column(Enum(JobType), default=JobType.OTHER)
    reservation_id = Column(Integer, ForeignKey('reservation.id'),
                            nullable=False)

    def cancel(self):
        rq.cancel_job(self.id)
        with db.auto_commit() as sess:
            sess.delete(self)


# Jobs
@rq.job(result_ttl=0)
@get_obj_from_id(Reservation)
def assign_spot(reservation):
    with db.auto_commit():
        reservation.assign_spot()

    for _ in range(current_app.config.get('LED_RETRY_TIMES', 2)):
        reservation.send_to_led()
        sleep(0.5)

    reservation.notify_user_start()


@rq.job(result_ttl=0)
@get_obj_from_id(Reservation)
def auto_checkin(reservation):
    with db.auto_commit(throw=False):
        reservation.checkin()


@rq.job(result_ttl=0)
@get_obj_from_id(Reservation)
def auto_checkout(reservation):
    with db.auto_commit(throw=False):
        reservation.checkout()
