# CMU Parking Backend

The API server for the cmu parking service.

## How to Run

To run this application locally, first install [redis](https://redis.io/). Then install dependencies with

```sh
$ make
```

Note that this command will install both application dependencies and development dependencies (like [pytest](https://docs.pytest.org/en/latest/) and [flake8](http://flake8.pycqa.org/en/latest/)). If you only want to run this application, use

```sh
$ make deps
```

After installing all dependencies, create a configuration file `config/app_conf.dev.py` for the app. Here is a sample of this file:

```python
SECRET_KEY = 'very-secret'
SQLALCHEMY_DATABASE_URI = ('postgresql://postgres:postgres'
                           '@localhost:5432/parking')
RQ_REDIS_URL = 'redis://localhost:6379/0'
UPLOAD_FOLDER = '/var/uploads'
TWILIO_NUMBER = '<NUMBER>'
TWILIO_SID = '<SID>'
TWILIO_TOKEN = '<TOKEN>'
```

Finally, run `redis-server` and

```sh
$ export FLASK_APP=app.py
$ export FLASK_ENV=development
$ flask rq worker & flask rq scheduler & flask run && fg && fg
```

The application will be ready on `localhost:5000`.

## How to Deploy

This application has been configured with [docker](https://www.docker.com/) and [ansible](https://www.ansible.com/). To deploy this application, modify `hosts`, `group_vars` and `site.yml` as needed, then run

```sh
$ ansible-playbook site.yml --ask-vault-pass --limit local --tags build
```

to build and push the docker image, and run

```sh
$ ansible-playbook site.yml --ask-vault-pass --limit heinz
```

to deploy the application to the server. It will install docker, copy configuration files, pull the docker images, run docker containers and configure NGINX properly.
