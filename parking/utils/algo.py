import collections as coll
import functools as ft
import itertools as it
from .shoyu import sorted_merge, first, rest


def interval_count(intervals, drop_empty=True):
    """Count the number of occurrences for all intervals.

    Given a list of intervals, this function will return a list
    of points that are the starting point of intervals, with the
    number of occurrence.

    Parameters
    ----------
    intervals
        An iterable object of tuples of 2 which represents the start
        and end of an interval.

    Returns
    -------
    list
        A list of tuples of 3. The first two elements of the tuple marks
        the start and end of an interval, and the second element is the
        number of occurrences of the interval.

    Examples
    --------
    This function can be used to transform a list of reservations into a
    plotting friendly format.

    >>> interval_count([(1, 2), (1, 3)])
    [(1, 2, 2), (2, 3, 1)]

    """
    xs = coll.Counter(map(first, intervals)).items()
    ys = [(k, -v) for k, v in coll.Counter(map(lambda x: first(rest(x)),
                                               intervals)).items()]
    xys = sorted_merge(xs, ys, key=first)

    def _fold(xs_l, x):
        _xs, _level = xs_l
        level = _level + x[1]
        xs = [*_xs, (x[0], level)]
        return xs, level

    xcs = first(ft.reduce(_fold, xys, ([], 0)))

    def _reshape(xy):
        x, y = xy
        return (x[0], y[0], x[1])

    def _is_empty(x):
        x1, x2, c = x
        return x1 == x2 or (drop_empty and c == 0)

    return list(it.filterfalse(_is_empty,
                               map(_reshape,
                                   zip(xcs, rest(xcs)))))


def calc_occupancy(beg, end, intervals):
    """Calculate occupancy of all intervals with in the range.

    This function will aggregate the number of occurrence of all
    possible intervals between `beg` and `end`, given a collection of
    occupied intervals.

    Parameters
    ----------
    beg
        Start of the range of interest.
    end
        End of the range of interest.
    intervals
        An iterable object of tuples of 2 which represents the start
        and end of an interval.

    Returns
    -------
    list
        A sorted list of tuples of 3. The first two elements of the tuple
        marks the start and end of an interval, and the second element is the
        number of occurrences of the interval.

    Examples
    --------
    Query the available slots within a certain range.

    >>> calc_occupancy(0, 10, [(1, 2), (2, 3), (2, 4)])
    [(0, 1, 0), (1, 2, 1), (2, 3, 2), (3, 4, 1), (4, 10, 0)]

    """
    ycs = interval_count([(beg, end), *intervals])
    xcs = map(lambda x: (x[0], x[1], x[2] - 1),
              filter(lambda x: x[0] >= beg and x[1] <= end, ycs))
    return list(xcs)
