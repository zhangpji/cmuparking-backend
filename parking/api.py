from flask import jsonify, Blueprint
from flask_restful import Api
from .resources import (
    Me,
    MyReservations, MyReservationsStats, MyReservation,
    Locations, LocationSpots, MaxDuration, Spot, Violations)

bp = Blueprint('api', __name__)
api = Api(bp)


@bp.route('/')
def index():
    return jsonify({
        'message': 'Welcome to CMU Parking!',
        'endpoints': [
            {'name': 'my profile',
             'endpoint': '/me'},
            {'name': 'my reservations',
             'endpoint': '/me/reservations'},
        ]
    })


api.add_resource(Me, '/me')
api.add_resource(MyReservations, '/me/reservations')
api.add_resource(MyReservationsStats, '/me/reservations/stats')
api.add_resource(MyReservation, '/me/reservations/<int:id>')
api.add_resource(Locations, '/locations')
api.add_resource(LocationSpots, '/locations/<int:id>/spots')
api.add_resource(MaxDuration, '/locations/<int:id>/duration')
api.add_resource(Spot, '/spots/<int:id>')
api.add_resource(Violations, '/violations')


def init_app(app):
    app.register_blueprint(bp, url_prefix='/api')
    return app
