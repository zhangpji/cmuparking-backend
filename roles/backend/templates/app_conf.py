SECRET_KEY = '{{ app_secret_key }}'
SQLALCHEMY_DATABASE_URI = ('postgresql://{{ db_user }}:{{ db_password }}'
                           '@postgres:5432/{{ db_name }}')
RQ_REDIS_URL = 'redis://redis:6379/0'
UPLOAD_FOLDER = '/var/uploads'
TWILIO_NUMBER = '{{ twilio_number }}'
TWILIO_SID = '{{ twilio_sid }}'
TWILIO_TOKEN = '{{ twilio_token }}'
