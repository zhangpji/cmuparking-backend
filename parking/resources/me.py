from flask_restful import Resource
from marshmallow import (
    Schema, fields, pre_load, post_load,
    ValidationError,
)
from marshmallow.validate import Length
from flask_jwt_extended import jwt_required
from twilio.base.exceptions import TwilioRestException
from ..auth import get_current_user_or_abort
from ..models import User
from ..signals import new_user
from ..jobs import msg
from ..utils.io import (
    load_from_request,
    parse_query_string,
    dump_data,
)


def is_unique_email(email):
    user = User.get_user_by_email(email)
    if user:
        raise ValidationError(f'Email address {email} has been used.')


def is_nonempty_string(data):
    if not data:
        raise ValidationError('Data not provided.')


def is_valid_phone_number(data):
    if data:
        try:
            msg.send_sms(to=data, body="Welcome to CMU Parking!")
        except TwilioRestException:
            raise ValidationError('Invalid phone number.')


def get_me_schema(avatar_size=None):
    class MeSchema(Schema):
        id = fields.Int(dumps_only=True)
        email = fields.Email(required=True, validate=is_unique_email)
        password = fields.Str(required=True,
                              validate=Length(min=8),
                              load_only=True)
        name = fields.Str()
        phone_number = fields.Str(validate=is_valid_phone_number)
        plate_number = fields.Str(required=True, validate=is_nonempty_string)
        avatar_url = fields.Function(lambda x: x.get_avatar(avatar_size),
                                     dumps_only=True)

        @pre_load
        def _unify_email(self, data):
            email = data.get('email')
            if email:
                data['email'] = email.lower()

            return data

    return MeSchema


class MeArgsSchema(Schema):
    avatar_size = fields.Int()

    @post_load
    def _unwrap(self, data):
        return data.get('avatar_size', 50)


class Me(Resource):
    @jwt_required
    def get(self):
        avatar_size = parse_query_string(MeArgsSchema)
        MeSchema = get_me_schema(avatar_size)
        me = get_current_user_or_abort()
        return dump_data(MeSchema, me)

    def post(self):
        data = load_from_request(get_me_schema())
        user = User.create(**data)

        new_user.send(user)

        return {'message': f'Created new user: {user.email}.'}
