from flask import request
from flask_restful import abort
from marshmallow import ValidationError, fields, utils


def get_json_from_request():
    json_data = request.get_json()
    if not json_data:
        abort(400, message='Non input data provided.')
    return json_data


def load_from_request(schema_cls, **kwargs):
    try:
        result = (schema_cls(strict=True)
                  .load(get_json_from_request(), **kwargs)
                  .data)
    except ValidationError as err:
        abort(422, message='Invalid data format.', errors=err.messages)

    return result


def parse_query_string(schema_cls, **kwargs):
    try:
        result = (schema_cls(strict=True)
                  .load(request.args, **kwargs)
                  .data)
    except ValidationError as err:
        abort(422, message='Invaliad query string.', errors=err.messages)

    return result


def dump_data(schema_cls, data, **kwargs):
    try:
        result = (schema_cls(strict=True)
                  .dump(data, **kwargs)
                  .data)
    except Exception:
        abort(500, message='Server error. Please contact us.')

    return result


class DelimitedListField(fields.List):
    def __init__(self,
                 cls_or_instance,
                 delimiter=None,
                 as_string=False,
                 **kwargs):
        self.delimiter = delimiter or ','
        self.as_string = as_string
        super().__init__(cls_or_instance, **kwargs)

    def _deserialize(self, value, attr, data):
        try:
            ret = (
                value
                if utils.is_iterable_but_not_string(value)
                else value.split(self.delimiter)
            )

        except AttributeError:
            self.fail('invalid')

        return super()._deserialize(ret, attr, data)
