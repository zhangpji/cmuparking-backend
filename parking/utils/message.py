from flask import current_app, _app_ctx_stack
from twilio.rest import Client


class Message:
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.teardown_appcontext(self.teardown)

    def teardown(self, exception):
        ctx = _app_ctx_stack.top

        if hasattr(ctx, 'twilio_client'):
            del ctx.twilio_client

    @property
    def twilio_client(self):
        ctx = _app_ctx_stack.top
        if ctx is not None:
            if not hasattr(ctx, 'twilio_client'):
                user_id = current_app.config.get('TWILIO_SID')
                token = current_app.config.get('TWILIO_TOKEN')
                ctx.twilio_client = Client(user_id, token)

            return ctx.twilio_client

    def send_sms(self, to, body, **kwargs):
        from_ = current_app.config.get('TWILIO_NUMBER')

        return self.twilio_client.messages.create(
            to=to, from_=from_, body=body, **kwargs)
