import datetime as dt
from flask_restful import Resource
from marshmallow import (
    Schema, fields, post_load, utils
)
from flask_jwt_extended import jwt_required
from ..models import (
    Location as Location_,
    Spot as Spot_,
)
from ..utils.io import (
    parse_query_string,
    dump_data,
)


class LocationsSchema(Schema):
    id = fields.Int(dumps_only=True)
    name = fields.Str(required=True)
    lat = fields.Float(required=True)
    lng = fields.Float(required=True)


class Locations(Resource):
    def get(self):
        return dump_data(LocationsSchema,
                         Location_.query.filter_by(visible=True),
                         many=True)


class LocationSpotsSchema(Schema):
    id = fields.Int(dumps_only=True)
    name = fields.Str(dumps_only=True)


class LocationSpots(Resource):
    def get(self, id):
        location = Location_.query.get(id)
        return dump_data(LocationSpotsSchema,
                         location.visible_spots,
                         many=True)


class MaxDurationArgsSchema(Schema):
    start = fields.DateTime()

    @post_load
    def _unwrap(self, data):
        return data.get('start', dt.datetime.utcnow()).replace(tzinfo=None)


class MaxDuration(Resource):
    @jwt_required
    def get(self, id):
        location = Location_.query.get(id)
        start = parse_query_string(MaxDurationArgsSchema)

        slot = location.get_nearest_slot(start)
        if slot:
            return {
                'messages': [],
                'duration': (slot[1] - slot[0]).total_seconds() // 60,
                'alternative': (None if slot[0] == start
                                else utils.isoformat(slot[0])),
            }
        else:
            return {
                'messages': ['No available time slots.'],
                'duration': 0,
                'alternative': None
            }, 422


class SpotSchema(Schema):
    id = fields.Int(dumps_only=True)
    location_id = fields.Int(dumps_only=True)
    name = fields.Str(dumps_only=True)


class Spot(Resource):
    @jwt_required
    def get(self, id):
        spot = Spot_.query.get(id)
        return dump_data(SpotSchema,
                         spot)
