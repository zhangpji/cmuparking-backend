---

- import_tasks: config.yml

- name: Prepare network
  docker_network:
    name: backend

- name: Start redis
  tags: [devel]
  docker_container:
    name: redis
    image: redis:3.2.11-alpine
    command: redis-server --appendonly yes
    state: started
    restart_policy: always
    ports:
      - "6379:6379"
    volumes:
      - "redis-data:/data"
    networks:
      - name: backend

- name: Start database
  tags: [devel]
  docker_container:
    name: postgres
    image: postgres:10.2-alpine
    restart_policy: always
    state: started
    env:
      POSTGRES_USER: "{{ db_user }}"
      POSTGRES_PASSWORD: "{{ db_password }}"
      POSTGRES_DB: "{{ db_name }}"
    ports:
      - "5432:5432"
    volumes:
      - "db-data:/var/lib/postgresql/data"
    networks:
      - name: backend

- name: Clear database
  when: cleardb
  docker_container:
    name: cleardb
    image: "{{ docker_username }}/parking-backend:{{ version }}"
    pull: yes
    auto_remove: yes
    command: ["sh", "-c", "'flask cleardb --yes'"]
    env:
      SERVER_SETTINGS: /usr/src/config/app_conf.py
    volumes:
      - "{{ config_dir }}:/usr/src/config"
    networks:
      - name: backend

- name: Prepare database
  docker_container:
    name: initdb
    image: "{{ docker_username }}/parking-backend:{{ version }}"
    pull: yes
    auto_remove: yes
    command: ["sh", "-c", "'flask initdb && flask add-admin --name {{ admin_name }} --email {{ admin_email}} --password {{ admin_password }}'"]
    env:
      SERVER_SETTINGS: /usr/src/config/app_conf.py
    volumes:
      - "{{ config_dir }}:/usr/src/config"
    networks:
      - name: backend

- name: Start backend
  docker_container:
    name: backend
    image: "{{ docker_username }}/parking-backend:{{ version }}"
    env:
      SERVER_SETTINGS: /usr/src/config/app_conf.py
      GUNICORN_SETTINGS: /usr/src/config/gunicorn_conf.py
    state: started
    restart: yes
    restart_policy: always
    ports:
      - "5000:5000"
    volumes:
      - "{{ config_dir }}:/usr/src/config"
      - "{{ upload_dir }}:/var/uploads"
    networks:
      - name: backend

- name: Start RQ worker
  docker_container:
    name: worker
    image: "{{ docker_username }}/parking-backend:{{ version }}"
    env:
      SERVER_SETTINGS: /usr/src/config/app_conf.py
    state: started
    command: "flask rq worker"
    restart: yes
    restart_policy: always
    volumes:
      - "{{ config_dir }}:/usr/src/config"
    networks:
      - name: backend

- name: Start RQ scheduler
  docker_container:
    name: scheduler
    image: "{{ docker_username }}/parking-backend:{{ version }}"
    env:
      SERVER_SETTINGS: /usr/src/config/app_conf.py
    state: started
    command: "flask rq scheduler"
    restart: yes
    restart_policy: always
    volumes:
      - "{{ config_dir }}:/usr/src/config"
    networks:
      - name: backend
