.POSIX:

.PHONY: all
all: dev

.PHONY: dev
dev: boot dev-deps.txt
	pip-sync dev-deps.txt

dev-deps.txt: boot deps.txt dev-deps.pip
	pip-compile --generate-hashes dev-deps.pip

.PHONY: deps
deps: boot deps.txt
	pip-sync deps.txt

deps.txt: boot deps.pip
	pip-compile --generate-hashes deps.pip

.PHONY: boot
boot:
	pip install pip-tools

.PHONY: test
test:
	python -m pytest tests
