import enum
import datetime as dt
from sqlalchemy import (
    Column,
    Integer, DateTime, Enum, String, Text,
    ForeignKey,
)
from .base import Base


class ViolationState(enum.Enum):
    PENDING = enum.auto()
    VERIFIED = enum.auto()
    DENIED = enum.auto()


class Violation(Base):
    __tablename__ = 'violation'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    updated_at = Column(DateTime, default=dt.datetime.utcnow,
                        onupdate=dt.datetime.utcnow)

    state = Column(Enum(ViolationState), default=ViolationState.PENDING)

    photo_id = Column(String(255), nullable=False)

    comment = Column(Text)

    customer_id = Column(Integer,
                         ForeignKey('user.id'),
                         nullable=False)
    spot_id = Column(Integer,
                     ForeignKey('spot.id'),
                     nullable=False)

    def is_pending(self):
        return self.state is ViolationState.PENDING

    def confirm(self):
        if self.is_pending():
            self.state = ViolationState.VERIFIED
        else:
            raise ValueError('invalid state transition')

    def reject(self):
        if self.is_pending():
            self.state = ViolationState.DENIED
        else:
            raise ValueError('invalid state transition')
