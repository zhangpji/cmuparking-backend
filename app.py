import os
import click
from parking import create_app
from parking.models import db, Admin

is_dev = os.getenv('FLASK_ENV') == 'development'

if is_dev:
    config_file = os.path.abspath('config/app_conf.dev.py')
    app = create_app(config_file)

    @app.after_request
    def add_headers(resp):
        resp.headers['Cache-Control'] = 'no-store'
        resp.headers['Pragma'] = 'no-cache'
        return resp
else:
    app = create_app()


@app.cli.command()
def initdb():
    db.create_all()


@app.cli.command()
@click.confirmation_option(help='This action cannot be undone. Continue?')
def cleardb():
    db.drop_all()
    click.echo('Cleared db.')


@app.cli.command('add-admin')
@click.option('--name', prompt='Admin username (for signing in)')
@click.option('--email', prompt='Admin email (for notification)')
@click.password_option()
def add_admin(name, email, password):
    if name and password:
        if Admin.query.filter_by(name=name).first() is None:
            admin = Admin.create(email, password, name)
            with db.auto_commit() as sess:
                sess.add(admin)
            click.echo(f'Admin account {name} ({email}) created.')
        else:
            click.echo(f'Admin {name} alreadly exists.')
