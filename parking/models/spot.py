import datetime as dt
from sqlalchemy import (
    Column,
    Integer, Float, DateTime, String, Boolean,
    ForeignKey, UniqueConstraint)
# FIXME Potential cyclic dependency
from .reservation import Reservation, ReservationState
from .base import db, Base
from ..utils.algo import calc_occupancy


class Conf(Base):
    __tablename__ = 'conf'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    location_id = Column(Integer, ForeignKey('location.id'),
                         nullable=False)
    reservation_padding = Column(Integer, default=15, nullable=False)
    time_zone = Column(String(50), default='US/Eastern', nullable=False)


class Location(Base):
    __tablename__ = 'location'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    updated_at = Column(DateTime, default=dt.datetime.utcnow,
                        onupdate=dt.datetime.utcnow)
    name = Column(String(255), nullable=False)

    lat = Column(Float, nullable=False)
    lng = Column(Float, nullable=False)

    spots = db.relationship('Spot', backref='location', lazy='dynamic')
    reservations = db.relationship('Reservation', backref='location',
                                   lazy='dynamic')

    confs = db.relationship('Conf', backref='location', lazy='dynamic')

    visible = Column(Boolean, default=True, nullable=False)

    def hide(self):
        self.visible = False
        self.name = f'{self.id}_{self.name}'

    @property
    def conf(self):
        conf = (self.confs
                .order_by(Conf.created_at.desc())
                .first())

        if conf:
            return conf
        else:
            new_conf = Conf(location_id=self.id)
            with db.auto_commit() as sess:
                sess.add(new_conf)
            return new_conf

    @property
    def visible_spots(self):
        return (self
                .spots
                .filter_by(visible=True))

    def get_available_slots(self,
                            beg,
                            max_duration=dt.timedelta(hours=10)):
        # Range of interest
        end = beg + max_duration

        # Left padding
        padding = dt.timedelta(
            minutes=self.conf.reservation_padding)

        # Get all reservations that intersect with the range
        reservations = (self
                        .reservations
                        .filter(
                            Reservation.state != ReservationState.CANCELLED,
                            Reservation.state != ReservationState.ENDED,
                            Reservation.end > beg))

        # Reshape data and apply padding
        intervals = [(x.start - padding, x.end) for x in reservations]

        # Calculate occupancies
        occupancies = calc_occupancy(beg, end, intervals)
        num_spots = self.visible_spots.count()

        # Get all available itervals
        available_slots = [(x[0], x[1])
                           for x in occupancies
                           if x[2] < num_spots]

        return available_slots

    def get_nearest_slot(self, beg, **kwargs):
        available_slots = self.get_available_slots(beg, **kwargs)

        if available_slots:
            result = available_slots[0]

            # If the current start is good, then continue
            if result[0] == beg:
                for slot in available_slots[1:]:
                    if slot[0] == result[1]:
                        result = (result[0], slot[1])
                    else:
                        break
                return result

            # Otherwise, adjust start time
            else:
                return self.get_nearest_slot(result[0], **kwargs)

        else:
            return None

    def get_available_spot(self, beg):
        for spot in self.visible_spots:
            num_rs = (spot
                      .reservations
                      .filter(
                          Reservation.end > beg,
                          Reservation.start <= beg,
                          Reservation.state != ReservationState.CANCELLED,
                          Reservation.state != ReservationState.ENDED)
                      .count())

            if num_rs == 0:
                return spot.id

        raise RuntimeError("no available spots")


class Spot(Base):
    __tablename__ = 'spot'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    updated_at = Column(DateTime, default=dt.datetime.utcnow,
                        onupdate=dt.datetime.utcnow)

    name = Column(String(10), nullable=False)

    ip_addr = Column(String(30), nullable=False)

    location_id = Column(Integer, ForeignKey('location.id'),
                         nullable=False)

    reservations = db.relationship('Reservation', backref='spot',
                                   lazy='dynamic')

    violations = db.relationship('Violation', backref='spot',
                                 lazy='dynamic')

    visible = Column(Boolean, default=True, nullable=False)

    # Avoid duplicate names in one parking lot
    __table_args__ = (UniqueConstraint('location_id', 'name', 'visible',
                                       name='spot_name'),)

    def __str__(self):
        return f'{self.location.name}, spot {self.name}'

    def hide(self):
        self.visible = False
        self.name = f'{self.id}_{self.name}'
