from flask import (
    Blueprint, jsonify)
from werkzeug.exceptions import UnprocessableEntity
from flask_jwt_extended import (
    JWTManager,
    create_access_token, create_refresh_token,
    jwt_required, jwt_refresh_token_required,
    current_user, get_raw_jwt)
from marshmallow import Schema, fields, pre_load
from .models import db, User, InvalidToken
from .utils.io import load_from_request


jwt = JWTManager()
bp = Blueprint('auth', __name__)


@jwt.user_identity_loader
def _convert_user_to_id(user):
    return user.id


@jwt.user_loader_callback_loader
def _load_user_from_jwt(identity):
    return User.query.get(identity)


@jwt.token_in_blacklist_loader
def _is_token_invalid(token):
    jti = token['jti']
    return InvalidToken.is_jti_invalid(jti)


@jwt.expired_token_loader
def _expired_token_callback():
    return jsonify({'message': 'Token has expired.'}), 401


@jwt.invalid_token_loader
def _invalid_token_callback(message):
    return jsonify({'message': message}), 422


@jwt.unauthorized_loader
def _unauthorized_callback(message):
    return jsonify({'message': message}), 401


@jwt.revoked_token_loader
def _revoked_token_callback(message):
    return jsonify({'message': 'Token has been revoked.'}), 401


class LoginSchema(Schema):
    email = fields.Email(required=True)
    password = fields.Str(required=True)

    @pre_load
    def _unify_email(self, data):
        email = data.get('email')
        if email:
            data['email'] = email


@bp.route('/new', methods=['POST'])
def login():
    error_resp = jsonify({'message': 'Wrong email or password.'}), 422

    try:
        data = load_from_request(LoginSchema)
    except UnprocessableEntity:
        return error_resp

    user = User.get_user_by_email(data['email'])
    if user and user.verify_password(data['password']):
        access_token = create_access_token(user)
        refresh_token = create_refresh_token(user)
        return jsonify({
            'message': f'Logged in as {user.email}.',
            'access_token': access_token,
            'refresh_token': refresh_token,
        })
    else:
        return error_resp


@bp.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh_token():
    return jsonify({'access_token': create_access_token(current_user)})


def logout():
    jti = InvalidToken(jti=get_raw_jwt()['jti'])
    with db.auto_commit() as sess:
        sess.add(jti)
    return jsonify({'message': 'Logged out.'})


revoke_access_token = jwt_required(logout)
revoke_refresh_token = jwt_refresh_token_required(logout)
bp.add_url_rule('/revoke', 'logout',
                revoke_access_token, methods=['DELETE'])
bp.add_url_rule('/revoke2', 'logout2',
                revoke_refresh_token, methods=['DELETE'])


def get_current_user_or_abort():
    if current_user:
        return current_user
    else:
        return jsonify({'message': 'User not found.'}), 401


def init_app(app):
    app.register_blueprint(bp, url_prefix='/auth')
    jwt.init_app(app)
    return app
