import datetime as dt
from hashlib import md5
import string
import random
from sqlalchemy import (
    Column,
    Integer, String, DateTime)
from werkzeug.security import (
    generate_password_hash,
    check_password_hash)
from .base import db, Base


def random_name():
    return ''.join(random.sample(string.ascii_letters, 10))


def get_gravatar(email, size):
    email_hash = md5(email.lower().encode('utf-8')).hexdigest()
    return ('https://www.gravatar.com/avatar/'
            f'{email_hash}?d=mm&s={size}')


# Normal users
class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    email = Column(String(255), unique=True, nullable=False, index=True)
    phone_number = Column(String(30))
    name = Column(String(100), default=random_name)
    _password = Column('password', String(100))
    plate_number = Column(String(30), nullable=False)

    reservations = db.relationship('Reservation', backref='customer',
                                   lazy='dynamic')

    violations = db.relationship('Violation', backref='customer',
                                 lazy='dynamic')

    def __repr__(self):
        return f'<User {self.name}>'

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, raw):
        self._password = generate_password_hash(raw)

    def verify_password(self, raw):
        if not self._password:
            return False
        return check_password_hash(self._password, raw)

    def get_avatar(self, size):
        return get_gravatar(self.email, size)

    @classmethod
    def get_user_by_email(cls, email):
        user = cls.query.filter_by(email=email).first()
        if user:
            return user

    @classmethod
    def create(cls, email, password, plate_number, phone_number, name=None):
        with db.auto_commit() as sess:
            user = cls(email=email,
                       plate_number=plate_number,
                       phone_number=phone_number,
                       name=name)
            user.password = password
            sess.add(user)

        return user


# Admin
class Admin(Base):
    __tablename__ = 'admin'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)
    email = Column(String(255), index=True)
    name = Column(String(100), unique=True, nullable=False)
    _password = Column('password', String(100))

    def __repr__(self):
        return f'<Admin {self.name}>'

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, raw):
        self._password = generate_password_hash(raw)

    def verify_password(self, raw):
        if not self._password:
            return False
        return check_password_hash(self._password, raw)

    def get_avatar(self, size=80):
        return get_gravatar(self.email, size)

    @classmethod
    def create(cls, email, password, name=None):
        admin = cls(email=email,
                    name=name)
        admin.password = password
        return admin
