import datetime as dt
from sqlalchemy import (
    Column,
    Integer, DateTime, Text)
from .base import db, Base


class Action(Base):
    __tablename__ = 'action'

    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=dt.datetime.utcnow)

    message = Column(Text, nullable=False)
    record = Column(Text, nullable=False)

    @classmethod
    def add(cls, message, record):
        record = cls(message=message,
                     record=record)
        with db.auto_commit() as sess:
            sess.add(record)
