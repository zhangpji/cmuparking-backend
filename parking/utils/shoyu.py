import functools as ft
import itertools as it
import datetime as dt
import pytz


class peekable:
    """Iterator with a new `peek` method."""
    def __init__(self, iterable):
        self._it = iter(iterable)
        self._cached = None

    def __bool__(self):
        try:
            self.peek()
        except StopIteration:
            return False

        return True

    def __iter__(self):
        return self._it

    def peek(self):
        if self._cached is None:
            self._cached = next(self._it)

        return self._cached

    def __next__(self):
        if self._cached is not None:
            x = self._cached
            self._cached = None
            return x

        return next(self._it)

    @classmethod
    def generator(cls, fn):
        @ft.wraps(fn)
        def wrapped(*args, **kwargs):
            return cls(fn(*args, **kwargs))

        return wrapped


def identity(x):
    return x


def first(xs):
    return peekable(xs).peek()


def rest(xs):
    return peekable(it.islice(xs, 1, None))


def interleave(xs, ys):
    return peekable(it.chain(*zip(xs, ys)))


def sorted_merge(xs_, ys_, reverse=False, **kwargs):
    """Merge two iterables in order.

    This function will merge two iterables into one generator.
    Note that when two elements tie, the element from the first
    iterable will always be prefered.
    """
    xs = peekable(sorted(xs_, reverse=reverse, **kwargs))
    ys = peekable(sorted(ys_, reverse=reverse, **kwargs))

    _key = kwargs.get('key', identity)

    def _cmp(x, y):
        if reverse:
            return _key(x) >= _key(y)
        else:
            return _key(x) <= _key(y)

    while xs or ys:
        if not xs:
            yield next(ys)
        elif not ys:
            yield next(xs)
        else:
            yield next(xs) if _cmp(xs.peek(), ys.peek()) else next(ys)


def dt_range(beg, end, step_):
    """Range for datetime."""
    if isinstance(step_, dt.timedelta):
        step = step_
    elif isinstance(step_, dict):
        step = dt.timedelta(**step_)
    else:
        raise TypeError('Step must be a dictionary or timedelta.')

    current = beg
    while current < end:
        yield current

        current += step


def strftime_local(time, tz_, format_):
    utc_time = pytz.utc.localize(time)
    tz = pytz.timezone(tz_)
    return utc_time.astimezone(tz).strftime(format_)
