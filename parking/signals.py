from blinker import Namespace


_signals = Namespace()

# User
new_user = _signals.signal('new-user')

# Reservation
new_reservation = _signals.signal('new-reservation')

# Violation
new_violation = _signals.signal('new-violation')
