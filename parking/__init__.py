import os
import uuid
from flask import (
    Flask as Flask_, json, jsonify, send_from_directory,
    request,
)
from . import models, api, auth, jobs, admin, guard, subscribers
from .utils.shoyu import strftime_local


def extract_extension(filename):
    parts = filename.split('.')
    return parts[-1] if len(parts) > 1 else ''


class Flask(Flask_):
    jinja_options = {
        'trim_blocks': True,
        'lstrip_blocks': True,
        'autoescape': True
    }


def _create_flask_app(config=None):
    app = Flask(__name__)

    # default settings
    app.config.from_object('parking.settings')

    # environment variable
    if 'SERVER_SETTINGS' in os.environ:
        app.config.from_envvar('SERVER_SETTINGS')

    # pyfiles
    if config is not None:
        app.config.from_pyfile(config)

    return app


def add_hooks(app):
    with open(app.config['ASSETS_INDEX'], 'r') as f:
        assets = json.load(f)

    @app.template_filter('strftime_local')
    def _strftime_local(time, format_):
        tz = app.config.get('SERVER_TIME_ZONE', 'US/Eastern')
        return strftime_local(time, tz, format_)

    @app.context_processor
    def _add_context():
        return {
            'assets': assets,
            'current_admin': admin.current_admin
        }

    # Uploads
    @app.route('/uploads/<filename>')
    def get_uploads(filename):
        return send_from_directory(app.config.get('UPLOAD_FOLDER'),
                                   filename)

    @app.route('/uploads', methods=['POST'])
    def upload():
        file = request.files.get('file')
        extension = file and extract_extension(file.filename)
        if extension in app.config.get('ALLOWED_UPLOAD_FILETYPES'):
            filename = f'{uuid.uuid4().hex}.{extension}'
            file.save(os.path.join(app.config.get('UPLOAD_FOLDER'), filename))
            return jsonify({'filename': filename})
        else:
            return jsonify({'message': 'Unallowed file type.'}), 400


def create_app(config=None):
    app = _create_flask_app(config)
    models.init_app(app)
    api.init_app(app)
    auth.init_app(app)
    jobs.init_app(app)
    admin.init_app(app)
    guard.init_app(app)
    subscribers.init_app(app)
    add_hooks(app)
    return app
