import pytest
from parking.utils import shoyu


def test_peekable():
    peekable = shoyu.peekable

    # Test basic functionalities
    it1 = peekable([1, 2])

    assert it1
    assert it1.peek() == 1
    assert next(it1) == 1
    assert it1.peek() == 2
    assert next(it1) == 2
    assert not it1
    with pytest.raises(StopIteration):
        next(it1)

    # Test generator
    @peekable.generator
    def gen(init):
        for x in range(init, init + 10):
            yield x

    it2 = gen(10)

    assert it2.peek() == 10
    assert next(it2) == 10
    assert next(it2) == 11


def test_sorted_merge():
    merge = shoyu.sorted_merge
    first = shoyu.first

    # ascending
    assert list(merge([1, 3, 7], [9, 2, 0])) == [0, 1, 2, 3, 7, 9]
    assert (list(merge([(1, 0), (2, 2)],
                       [(1, 1), (3, 3)],
                       key=first)) == [(1, 0), (1, 1), (2, 2), (3, 3)])

    # descending
    assert list(merge([1, 3, 7], [9, 2, 0], reverse=True)) == [9, 7, 3, 2, 1, 0]
    assert (list(merge([(1, 0), (2, 2)],
                       [(1, 1), (3, 3)],
                       key=first,
                       reverse=True)) == [(3, 3), (2, 2), (1, 0), (1, 1)])
