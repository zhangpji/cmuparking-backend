from pathlib import Path

ROOT = Path(__file__).resolve().parent

DEBUG = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

PER_PAGE = 10

ASSETS_INDEX = (ROOT / 'static' / 'assets.json').as_posix()

ALLOWED_UPLOAD_FILETYPES = ['png', 'jpg', 'jpeg']

LED_RETRY_TIMES = 10

SERVER_TIME_ZONE = 'US/Eastern'
