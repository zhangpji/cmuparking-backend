from sqlalchemy import (
    Column,
    Integer, String)
from .base import Base


class InvalidToken(Base):
    __tablename__ = 'invalid_token'

    id = Column(Integer, primary_key=True)
    jti = Column(String(255))

    @classmethod
    def is_jti_invalid(cls, jti):
        _jti = cls.query.filter_by(jti=jti).first()
        return bool(_jti)
