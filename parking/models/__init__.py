# flake8: noqa
from .base import db
from .user import User, Admin
from .token import InvalidToken
from .reservation import Reservation, Job
from .spot import Location, Spot, Conf
from .violation import Violation
from .action import Action


def init_app(app):
    db.init_app(app)
    return app
