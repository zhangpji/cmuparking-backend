from flask_cors import CORS
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address


cors = CORS()
limiter = Limiter(key_func=get_remote_address,
                  default_limits=['7000 per day', '500 per hour'])


def init_app(app):
    cors.init_app(app)
    limiter.init_app(app)
    return app
