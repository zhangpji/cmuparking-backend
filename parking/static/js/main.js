'use strict'

var init = function () {
  var dismissFlash = function () {
    var flash = window.document.getElementById('flash')
    if (flash) {
      flash.outerHTML = ''
    }
  }

  var flashCloseEl = window.document.getElementById('flash-close')
  if (flashCloseEl) {
    flashCloseEl.addEventListener(
      'click',
      function (e) {
        e.preventDefault()
        dismissFlash()
      }
    )
  }
  window.document.getElementById('flash-close')
  setTimeout(dismissFlash, 5000)
}

window.document.body.onload = init
