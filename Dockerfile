FROM centos:7

ENV LC_ALL=en_US.utf8

RUN yum -y update; yum clean all
RUN yum -y install epel-release; yum clean all
RUN yum -y install https://centos7.iuscommunity.org/ius-release.rpm; yum clean all
RUN yum -y install python36u python36u-pip; yum clean all
RUN ln -s /usr/bin/pip3.6 /usr/bin/pip

WORKDIR /app

COPY deps.txt deps.txt
RUN pip install pip-tools
RUN pip-sync deps.txt

COPY parking parking
COPY app.py ./

ENV FLASK_APP app.py

COPY vendor vendor
RUN chmod +x vendor/LED/Ledcom
ENV LED_PATH /app/vendor/LED

EXPOSE 5000
CMD ["sh", "-c", "gunicorn -c $GUNICORN_SETTINGS app:app"]
