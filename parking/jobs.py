import os
import subprocess
import datetime as dt
from time import sleep
from functools import wraps
from flask import current_app
from rq.job import Job, cancel_job
from rq.exceptions import NoSuchJobError
from flask_rq2 import RQ as RQ_
from .utils.message import Message


class RQ(RQ_):
    def get_job(self, job_id):
        return Job.fetch(job_id, connetction=self.connection)

    def cancel_job(self, job_id):
        try:
            cancel_job(job_id, connection=self.connection)
        except NoSuchJobError as e:
            current_app.logger.debug(str(e))


rq = RQ()
msg = Message()


def get_obj_from_id(table):
    def wrapper(fn):
        @wraps(fn)
        def wrapped(id):
            obj = table.query.get(id)
            return fn(obj)

        return wrapped

    return wrapper


def schedule_or_enqueue(job, time, *args, **kwargs):
    """Schedule or enqueue a job.

    Schedule a job at a time. If the given time is already passed,
    then the job will be directly enqueued. Returns the job id.
    """
    if time < dt.datetime.utcnow():
        job_ = job.queue(*args, **kwargs)
    else:
        job_ = job.schedule(time, *args, **kwargs)

    return job_.get_id()


# LED
def display_led(ip_addr, line, message):
    bin_path = os.environ.get('LED_PATH')
    if bin_path:
        try:
            subprocess.run(
                f'./Ledcom "{ip_addr}" "{line}" "{message}"',
                cwd=bin_path,
                shell=True,
                check=True,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            if current_app:
                current_app.logger.error(
                    'Failed (%d) to send message %s to LED %s: %s',
                    e.returncode, message, ip_addr, e.stderr)
            else:
                raise e


@rq.job(result_ttl=0)
def clear_led(ip_addr):
    for _ in range(current_app.config.get('LED_RETRY_TIMES', 2)):
        display_led(ip_addr, 1, 'Available')
        sleep(0.5)
        display_led(ip_addr, 2, '')
        sleep(0.5)


def init_app(app):
    rq.init_app(app)
    rq.init_cli(app)
    msg.init_app(app)
    return app
