bind = '0.0.0.0:5000'
workers = 2

timeout = 30

max_requests = 1000
max_requests_jitter = 500

proc_name = 'server'

accesslog = '-'
errorlog = '-'
loglevel = 'warning'
