import datetime as dt
from collections import defaultdict
from calendar import monthrange
from functools import wraps
import pytz
from flask import (
    Blueprint, current_app,
    g, session, request, url_for, redirect,
    render_template, flash)
from werkzeug.local import LocalProxy
from flask_wtf import FlaskForm
from flask_wtf._compat import string_types
from wtforms.widgets import HiddenInput
from wtforms.fields import (
    StringField, PasswordField, FloatField, IntegerField)
from wtforms.validators import DataRequired, NumberRange, StopValidation
from sqlalchemy import extract
from .models import (
    db, User, Reservation, Location, Spot, Admin, Violation, Action, Conf)


bp = Blueprint('admin', __name__)


# Helpers
def calc_stats(xs):
    now = dt.datetime.utcnow()
    count = defaultdict(lambda: 0)

    for x in xs:
        count[x.created_at.day] += 1

    full_count = {d: count.get(d, 0) for d in range(1, now.day + 1)}

    return {
        'total': len(xs),
        'range': (monthrange(now.year, now.month)[1],
                  max([*full_count.values(), 20])),
        'line': ' '.join([f'{d},{c}'
                          for d, c in full_count.items()])
    }


def merge_ranges(xys, merge_fn=max):
    return (merge_fn([x for x, _ in xys]),
            merge_fn([y for _, y in xys]))


# Session management
def admin_login(admin, permanent=True):
    session['sid'] = admin.id
    session.permanent = permanent
    g.current_admin = admin


def admin_logout():
    if 'sid' in session:
        del session['sid']


def get_current_admin():
    admin = g.get('current_admin', None)
    if admin:
        return admin

    sid = session.get('sid')
    if not sid:
        return None

    admin = Admin.query.get(sid)
    if not admin:
        admin_logout()
        return None
    g.current_admin = admin
    return admin


current_admin = LocalProxy(get_current_admin)


def admin_required(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        if not current_admin:
            url = url_for('admin.login', next=request.path)
            return redirect(url)
        return fn(*args, **kwargs)

    return wrapped


# Forms
class BaseForm(FlaskForm):
    def get_visible_fields(self):
        for field in self._fields:
            if isinstance(field, string_types):
                field = getattr(self, field, None)
            if field and not isinstance(field.widget, HiddenInput):
                yield field


class ActionForm(BaseForm):
    pass


class LoginForm(BaseForm):
    username = StringField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._admin = None

    def validate_password(self, field):
        name = self.username.data
        admin = Admin.query.filter_by(name=name).first()

        if not admin or not admin.verify_password(field.data):
            raise StopValidation('User name or password is incorrect.')

        self._admin = admin

    def login(self):
        if self._admin:
            admin_login(self._admin, True)


class LocationForm(BaseForm):
    name = StringField(validators=[DataRequired()])
    lat = FloatField('Latitude', validators=[DataRequired(),
                                             NumberRange(-90, 90)])
    lng = FloatField('Longitude', validators=[DataRequired(),
                                              NumberRange(-180, 180)])


class SpotForm(BaseForm):
    name = StringField(validators=[DataRequired()])
    ip_addr = StringField(validators=[DataRequired()])


class ConfForm(BaseForm):
    reservation_padding = IntegerField(label='Reservation Padding (Minutes)',
                                       validators=[DataRequired(),
                                                   NumberRange(0)])
    time_zone = StringField(label='Time Zone')

    def validate_time_zone(self, field):
        try:
            pytz.timezone(field.data)
        except pytz.exceptions.UnknownTimeZoneError:
            raise StopValidation(f'Unknown time zone {field.data}.')


# Routes
@bp.route('/')
@admin_required
def home():
    this_month = dt.datetime.utcnow().month
    _name_table = {
        'users': User,
        'reservations': Reservation,
        'violations': Violation,
    }
    stats = {
        name: calc_stats(table
                         .query
                         .filter(extract('month',
                                         table.created_at) == this_month)
                         .all())
        for name, table in _name_table.items()
    }
    range_ = merge_ranges([stat['range'] for stat in stats.values()])
    records = (Action
               .query
               .order_by(Action.created_at.desc())
               .limit(10)
               .all())
    return render_template('admin/home.j2',
                           active_page='home',
                           stats=stats,
                           range=range_,
                           records=records,
                           month=dt.datetime.now().strftime('%B'))


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_admin:
        return redirect(url_for('admin.home'))

    form = LoginForm()
    if form.validate_on_submit():
        form.login()
        return redirect(request.args.get('next', url_for('admin.home')))
    return render_template('admin/login.j2', form=form)


@bp.route('/logout')
def logout():
    admin_logout()
    return redirect(url_for('admin.home'))


@bp.route('/users')
@admin_required
def list_users():
    page_num = request.args.get('page', 1, type=int)
    per_page = current_app.config.get('PER_PAGE')
    users_page = (User
                  .query
                  .order_by(User.id)
                  .paginate(page_num, per_page, False))
    return render_template('admin/users.j2',
                           users=users_page.items,
                           pagination=users_page,
                           active_page='user')


@bp.route('/reservations')
@admin_required
def list_reservations():
    page_num = request.args.get('page', 1, type=int)
    per_page = current_app.config.get('PER_PAGE')
    reservations_page = (Reservation
                         .query
                         .order_by(Reservation.id)
                         .paginate(page_num, per_page, False))
    return render_template('admin/reservations.j2',
                           reservations=reservations_page.items,
                           pagination=reservations_page,
                           active_page='reservation')


@bp.route('/reservations/<int:id>/show')
@admin_required
def show_reservation(id):
    reservation = Reservation.query.get(id)
    reservation.send_to_led()
    return redirect(url_for('admin.list_reservations'))


@bp.route('/locations')
@admin_required
def list_locations():
    page_num = request.args.get('page', 1, type=int)
    per_page = current_app.config.get('PER_PAGE')
    locations_page = (Location
                      .query
                      .filter_by(visible=True)
                      .order_by(Location.id)
                      .paginate(page_num, per_page, False))
    form = ActionForm()
    return render_template('admin/locations.j2',
                           locations=locations_page.items,
                           pagination=locations_page,
                           active_page='location',
                           form=form)


@bp.route('/locations/add', methods=['GET', 'POST'])
@admin_required
def add_location():
    form = LocationForm()
    if form.validate_on_submit():
        location = Location(name=form.name.data,
                            lat=form.lat.data,
                            lng=form.lng.data)
        with db.auto_commit() as sess:
            sess.add(location)

        flash((f'Location {form.name.data} has been added. '
               'Add a few spots for it.'),
              'success')

        return redirect(url_for('admin.list_spots', id=location.id))

    return render_template('admin/location.j2',
                           form=form,
                           active_page='location')


@bp.route('/locations/<int:id>/edit', methods=['GET', 'POST'])
@admin_required
def edit_location(id):
    location = Location.query.get(id)
    form = LocationForm(obj=location)
    if form.validate_on_submit():
        with db.auto_commit():
            location.name = form.name.data
            location.lat = form.lat.data
            location.lng = form.lng.data

        flash('Location information has been updated.', 'success')

        return redirect(url_for('admin.list_locations'))

    return render_template('admin/location.j2',
                           form=form,
                           location=location,
                           active_page='location')


@bp.route('/locations/<int:id>/delete', methods=['POST'])
@admin_required
def delete_location(id):
    form = ActionForm()

    if form.validate_on_submit():
        location = Location.query.get(id)
        name = location.name
        with db.auto_commit():
            location.hide()

        flash(f'Location {name} has been deleted.', 'success')

    return redirect(url_for('admin.list_locations'))


@bp.route('/locations/<int:id>/spots')
@admin_required
def list_spots(id):
    page_num = request.args.get('page', 1, type=int)
    per_page = current_app.config.get('PER_PAGE')
    location = Location.query.get(id)
    spots_page = (location
                  .visible_spots
                  .order_by(Spot.id)
                  .paginate(page_num, per_page, False))
    form = SpotForm()
    deletion_form = ActionForm()
    return render_template('admin/spots.j2',
                           location=location,
                           spots=spots_page.items,
                           pagination=spots_page,
                           form=form,
                           dform=deletion_form,
                           active_page='location')


@bp.route('/locations/<int:id>/spots/add', methods=['POST'])
@admin_required
def add_spot(id):
    form = SpotForm()
    if form.validate_on_submit():
        spot = Spot(location_id=id,
                    name=form.name.data,
                    ip_addr=form.ip_addr.data)
        with db.auto_commit() as sess:
            sess.add(spot)

        flash(f'Spot {spot.name} has been added.', 'success')

    return redirect(url_for('admin.list_spots', id=id))


@bp.route('/locations/<int:lid>/spots/<int:sid>/delete', methods=['POST'])
@admin_required
def delete_spot(lid, sid):
    form = ActionForm()

    if form.validate_on_submit():
        spot = Spot.query.get(sid)
        name = spot.name
        with db.auto_commit():
            spot.hide()

        flash(f'Spot {name} has been deleted.', 'success')

    return redirect(url_for('admin.list_spots', id=lid))


@bp.route('/violations')
@admin_required
def list_violations():
    page_num = request.args.get('page', 1, type=int)
    per_page = current_app.config.get('PER_PAGE')
    violations_page = (Violation
                       .query
                       .order_by(Violation.state,
                                 Violation.created_at)
                       .paginate(page_num, per_page, False))
    confirmation_form = ActionForm()
    rejection_form = ActionForm()
    return render_template('admin/violations.j2',
                           violations=violations_page.items,
                           pagination=violations_page,
                           cform=confirmation_form,
                           rform=rejection_form,
                           active_page='violation')


@bp.route('/violations/<int:id>')
@admin_required
def view_violation(id):
    violation = Violation.query.get(id)
    return render_template('admin/violation.j2',
                           violation=violation,
                           backurl=(request.args.get('next')
                                    or url_for('admin.list_violations')),
                           active_page='violation')


@bp.route('/violations/<int:id>/confirm', methods=['POST'])
@admin_required
def confirm_violation(id):
    violation = Violation.query.get(id)
    form = ActionForm()

    if form.validate_on_submit():
        with db.auto_commit():
            violation.confirm()

        flash(f'Violation report {id} has been confirmed.', 'success')

    return redirect(url_for('admin.list_violations'))


@bp.route('/violations/<int:id>/reject', methods=['POST'])
@admin_required
def reject_violation(id):
    violation = Violation.query.get(id)
    form = ActionForm()

    if form.validate_on_submit():
        with db.auto_commit():
            violation.reject()

        flash(f'Violation report {id} has been rejected.', 'success')

    return redirect(url_for('admin.list_violations'))


@bp.route('/settings')
def list_confs():
    locations = Location.query.filter_by(visible=True)
    forms = [ConfForm(obj=location.conf)
             for location in locations]
    lfs = zip(locations, forms)
    return render_template('admin/settings.j2',
                           lfs=lfs,
                           active_page='setting')


@bp.route('/locations/<int:id>/settings', methods=['POST'])
def edit_conf(id):
    form = ConfForm()
    if form.validate_on_submit():
        new_conf = Conf(location_id=id,
                        reservation_padding=form.reservation_padding.data,
                        time_zone=form.time_zone.data)
        with db.auto_commit() as sess:
            sess.add(new_conf)

        flash('App settings have been updated.')

    return redirect(url_for('admin.list_confs'))


def init_app(app):
    app.register_blueprint(bp, url_prefix='/admin')
    return app
