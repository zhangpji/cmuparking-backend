from .signals import (
    new_user, new_reservation, new_violation)
from .models import Action


def record_new_user(user, **kwargs):
    Action.add((f'{user.name} ({user.email}) registered.'),
               f'N:U:{user.id}')


def record_new_reservation(reservation, **kwargs):
    Action.add((f'{reservation.customer.name} ({reservation.customer.email}) '
                f'created a new reservation at {reservation.location.name}.'),
               (f'N:R:{reservation.customer.id}:{reservation.id}'
                f':{reservation.location.id}'))


def record_new_violation(violation, **kwargs):
    Action.add((f'{violation.customer.name} ({violation.customer.email}) '
                f'reported new violation at {violation.spot.location.name}, '
                f'spot {violation.spot.name}.'),
               (f'N:V:{violation.customer.id}:{violation.id}'
                f':{violation.spot.id}'))


new_user.connect(record_new_user)
new_reservation.connect(record_new_reservation)
new_violation.connect(record_new_violation)


# Placeholder
def init_app(app):
    return app
