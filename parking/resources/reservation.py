import enum
from functools import reduce
from flask_restful import Resource
from marshmallow import (
    Schema, fields, post_load)
from marshmallow_enum import EnumField
from flask_jwt_extended import jwt_required
from ..auth import get_current_user_or_abort
from sqlalchemy import or_
from ..models import (
    db,
    Reservation,
    reservation as r_)
from ..utils.io import (
    load_from_request,
    parse_query_string,
    dump_data,
    DelimitedListField)


class MyReservationsSchema(Schema):
    id = fields.Int(dumps_only=True)
    location_id = fields.Int(required=True)
    spot_name = fields.Function(lambda x: x.spot.name,
                                dumps_only=True)
    state = EnumField(r_.ReservationState, dumps_only=True)
    start = fields.DateTime(required=True)
    end = fields.DateTime(required=True)


class MyReservationsArgsSchema(Schema):
    start = fields.Int()
    num = fields.Int()
    states = DelimitedListField(EnumField(r_.ReservationState))
    locations = DelimitedListField(fields.Int())

    @post_load
    def _unwrap(self, data):
        # pagination related
        start = data.get('start', 0)
        stop = start + data.get('num', 20)
        range_ = (start, stop)

        # filter
        states = data.get('states', [])
        locations = data.get('locations', [])

        filter_ = (
            or_(*[Reservation.state == state
                  for state in states]),

            or_(*[Reservation.location_id == location
                  for location in locations])
        )

        return range_, filter_


class MyReservations(Resource):
    @jwt_required
    def get(self):
        user = get_current_user_or_abort()
        range_, filter_ = parse_query_string(MyReservationsArgsSchema)
        reservations = (
            user.reservations
            .filter(*filter_)
            .order_by(Reservation.start))
        total_num = reservations.count()
        result = reservations.slice(*range_).all()
        return {
            'is_last': range_[1] >= total_num,
            'reservations': dump_data(MyReservationsSchema,
                                      result,
                                      many=True)
        }

    @jwt_required
    def post(self):
        user = get_current_user_or_abort()
        data = load_from_request(MyReservationsSchema)
        Reservation.create(customer_id=user.id, **data)

        return {'message': 'Reservation created successfully.'}


def get_stats(user):
    reservations = user.reservations.all()
    count_by_state = reduce(
        lambda cs, x: {
            **cs,
            x.state.name: cs.get(x.state.name, 0) + 1,
        },
        reservations,
        {})
    count_by_location = reduce(
        lambda cs, x: {
            **cs,
            x.location_id: cs.get(x.location_id, 0) + 1,
        },
        reservations,
        {})

    return {
        'by_state': count_by_state,
        'by_location': count_by_location,
    }


class MyReservationsStats(Resource):
    @jwt_required
    def get(self):
        user = get_current_user_or_abort()
        return get_stats(user)


class MyReservationSchema(Schema):
    extension = fields.DateTime()

    @post_load
    def _unwrap(self, data):
        return data.get('extension')


class _Mode(enum.Enum):
    CHECKIN = enum.auto()
    CHECKOUT = enum.auto()
    EXTEND = enum.auto()


class MyReservationArgsSchema(Schema):
    mode = EnumField(_Mode, required=True, loads_only=True)

    @post_load
    def _unwrap(self, data):
        return data.get('mode')


class MyReservation(Resource):
    @jwt_required
    def patch(self, id):
        mode = parse_query_string(MyReservationArgsSchema)
        reservation = (get_current_user_or_abort()
                       .reservations
                       .filter_by(id=id)
                       .first())

        with db.auto_commit():
            if mode is _Mode.CHECKIN:
                reservation.checkin()
            elif mode is _Mode.CHECKOUT:
                reservation.checkout()
            else:
                extension = load_from_request(MyReservationSchema)
                reservation.extend(extension)

        return {'message': f'{mode.name.capitalize()} finished.'}

    @jwt_required
    def delete(self, id):
        reservation = (get_current_user_or_abort()
                       .reservations
                       .filter_by(id=id)
                       .first())

        with db.auto_commit():
            reservation.cancel()

        return {'message': 'The reservation has been cancelled.'}
