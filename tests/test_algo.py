from parking.utils import algo


def test_interval_count():
    interval_count = algo.interval_count

    assert interval_count([(1, 2), (1, 3)]) == [(1, 2, 2), (2, 3, 1)]
    assert interval_count([(1, 2), (2, 3)]) == [(1, 2, 1), (2, 3, 1)]
    assert (interval_count([(1, 3), (2, 4), (3, 5)])
            ==
            [(1, 2, 1), (2, 3, 2), (3, 4, 2), (4, 5, 1)])
    assert interval_count([(1, 2), (4, 7)]) == [(1, 2, 1), (4, 7, 1)]


def test_calc_occupany():
    calc_occupancy = algo.calc_occupancy

    assert (calc_occupancy(0, 10, [(1, 2), (2, 3), (2, 4)])
            ==
            [(0, 1, 0), (1, 2, 1), (2, 3, 2), (3, 4, 1), (4, 10, 0)])
